class CreateComments < ActiveRecord::Migration[5.2]
  def change
    create_table :comments do |t|
      t.text :content
      t.references :commentable, polymorphic: true, index: true
      t.references :creator, polymorphic: true, index: true
      t.integer :comments_count, index: true, default: 0
      t.references :list, index: true
      t.timestamps
    end
  end
end
