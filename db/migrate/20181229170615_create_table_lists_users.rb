class CreateTableListsUsers < ActiveRecord::Migration[5.2]
  def change
    create_table :lists_users do |t|
      t.references :list
      t.references :user
    end
    add_index :lists_users, [:list_id, :user_id], unique: true
  end
end
