require 'rails_helper'

RSpec.describe 'Authentications', type: :request do
  describe 'POST /sign_in' do
    context 'With valid params' do
      it 'Sign in successfully' do
        user = FactoryBot.create(:member)
        post sign_in_path, params: { email: user.email, password: user.password }
        expect(response).to have_http_status(:success)
        expect(JSON.parse(response.body)['auth_token']).to eq(user.auth_token)
      end
    end

    context 'With Invalid params' do
      it 'Render error' do
        user = FactoryBot.create(:member)
        post sign_in_path, params: { email: user.email, password: 'wrong' }
        expect(response).to have_http_status(:unauthorized)
        expect(JSON.parse(response.body)['error']['message']).to eq('invalid credentials')
      end
    end

    context 'With No params' do
      it 'Render error' do
        post sign_in_path
        expect(response).to have_http_status(:unprocessable_entity)
        expect(JSON.parse(response.body)['error']['message']).to eq('No email or password')
      end
    end
  end

  describe 'POST /sign_up' do
    context 'With valid params' do
      it 'Member Sign up successfully' do
        post sign_up_path, params: { user: { email: 'test@board.com', password: '123456789', username: 'test1', type: 'Member' } }
        expect(response).to have_http_status(:created)
        user = User.last
        expect(user).to_not eq(nil)
        expect(JSON.parse(response.body)['auth_token']).to eq(user.auth_token)
      end

      it 'Admin Sign up successfully' do
        post sign_up_path, params: { user: { email: 'test@board.com', password: '123456789', username: 'test1', type: 'Admin' } }
        expect(response).to have_http_status(:created)
        user = User.last
        expect(user).to_not eq(nil)
        expect(JSON.parse(response.body)['auth_token']).to eq(user.auth_token)
      end
    end

    context 'With Invalid params' do
      it 'Render error' do
        post sign_up_path, params: { user: { email: 'test@board.com', password: '123456789', username: 'test1' } }
        expect(response).to have_http_status(:unprocessable_entity)
        expect(JSON.parse(response.body)['error']['message']).to eq("Type can't be blank")
      end
    end

    context 'With No params' do
      it 'Render error' do
        post sign_up_path
        expect(response).to have_http_status(:unprocessable_entity)
        expect(JSON.parse(response.body)['error']['message']).to eq('param is missing or the value is empty')
      end
    end
  end
end
