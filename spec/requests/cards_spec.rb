require 'rails_helper'

RSpec.describe "Cards", type: :request do
  describe "GET /cards" do
    context 'As an Admin' do
      it "Reads all cards" do
        admin1 = FactoryBot.create(:admin)
        cards = FactoryBot.create_list(:card, 15, creator: admin1)
        FactoryBot.create(:comment, commentable: cards[0], creator: admin1)
        get cards_path, headers: { 'Authorization': admin1.auth_token }
        expect(response).to have_http_status(:success)
        response_body = JSON.parse(response.body)
        expect(response_body['cards'].length).to eq(10)
        expect(response_body['cards'][0]['title']).to eq(cards[0]['title'])
        expect(response_body['total_pages']).to eq(2)
        expect(response_body['page']).to eq(1)
      end

      it "Reads cards orderd by main content" do
        admin1 = FactoryBot.create(:admin)
        list1  = FactoryBot.create(:list, admin: admin1)
        member = FactoryBot.create(:member)
        card_1 = FactoryBot.create(:card, list: list1, creator: admin1)
        card_2 = FactoryBot.create(:card, list: list1, creator: admin1)
        card_3 = FactoryBot.create(:card, list: list1, creator: admin1)
        FactoryBot.create_list(:card, 20, list: list1, creator: admin1)
        FactoryBot.create_list(:comment, 5, commentable: card_2, creator: member)
        FactoryBot.create_list(:comment, 2, commentable: card_3, creator: member)
        FactoryBot.create(:comment, commentable: card_1, creator: member)
        # Add subcomments to a comment in the second card to make sure orderd is by main comments only
        card3_first_comment = card_3.comments.first
        FactoryBot.create_list(:comment, 5, commentable: card3_first_comment, creator: member)
        get cards_path, headers: { 'Authorization': admin1.auth_token }
        response_body = JSON.parse(response.body)
        expect(response_body['total_pages']).to eq(3)
        expect(response_body['page']).to eq(1)
        expect(response_body['cards'][0]['title']).to eq(card_2.title)
        expect(response_body['cards'][0]['comments_count']).to eq(5)
        expect(response_body['cards'][1]['title']).to eq(card_3.title)
        expect(response_body['cards'][1]['comments_count']).to eq(2)
        expect(response_body['cards'][2]['title']).to eq(card_1.title)
        expect(response_body['cards'][2]['comments_count']).to eq(1)
      end
    end

    context "As a member" do
      it "Read only his cards" do
        admin1 = FactoryBot.create(:admin)
        member = FactoryBot.create(:member)
        member2 = FactoryBot.create(:member)
        list1  = FactoryBot.create(:list, admin: admin1)
        list2  = FactoryBot.create(:list, admin: admin1)
        list1.members << member
        list2.members << member
        card1 = FactoryBot.create(:card, creator: member, list: list1)
        card2 = FactoryBot.create(:card, creator: member, list: list2)
        FactoryBot.create(:card, creator: member2, list: list1)
        get cards_path, headers: { 'Authorization': member.auth_token }
        expect(response).to have_http_status(:success)
        response_body = JSON.parse(response.body)
        expect(response_body['cards'].length).to eq(2)
        expect(response_body['cards'][0]['title']).to eq(card2.title)
        expect(response_body['cards'][1]['title']).to eq(card1.title)
      end
    end

    context "As a guest" do
      it "Render unauthorized" do
        FactoryBot.create(:list)
        get cards_path
        expect(response).to have_http_status(:unauthorized)
      end
    end
  end

  describe "POST /cards" do
    context 'As an Admin' do
      it "Create Card on his own list" do
        admin1 = FactoryBot.create(:admin)
        list = FactoryBot.create(:list, admin: admin1)
        post cards_path, headers: { 'Authorization': admin1.auth_token }, params: { card: { title: 'test title', description: 'title description', list_id: list.id } }

        expect(response).to have_http_status(:created)
        response_body = JSON.parse(response.body)
        expect(response_body['title']).to eq('test title')
      end

      it "Create Card on another admin list" do
        admin1 = FactoryBot.create(:admin)
        list = FactoryBot.create(:list)
        post cards_path, headers: { 'Authorization': admin1.auth_token }, params: { card: { title: 'test title', description: 'title description', list_id: list.id } }

        expect(response).to have_http_status(:created)
        response_body = JSON.parse(response.body)
        expect(response_body['title']).to eq('test title')
      end
    end

    context "As a member" do
      it "Create card on assigned list" do
        list = FactoryBot.create(:list)
        member = FactoryBot.create(:member)
        list.members << member
        post cards_path, headers: { 'Authorization': member.auth_token }, params: { card: { title: 'test title', description: 'title description', list_id: list.id } }
        expect(response).to have_http_status(:created)
        response_body = JSON.parse(response.body)
        expect(response_body['title']).to eq('test title')
      end

      it "Render unauthorized for not assigned list" do
        member = FactoryBot.create(:member)
        list = FactoryBot.create(:list)
        post cards_path, headers: { 'Authorization': member.auth_token }, params: { card: { title: 'test title', description: 'title description', list_id: list.id } }
        expect(response).to have_http_status(:unauthorized)
      end
    end

    context "As a guest" do
      it "Render unauthorized" do
        list = FactoryBot.create(:list)
        post cards_path, params: { list: { title: 'test title', description: 'title description', list_id: list.id } }
        expect(response).to have_http_status(:unauthorized)
      end
    end
  end

  describe "Update /card/:id" do
    context 'As an Admin' do
      it "Update his own card" do
        admin1 = FactoryBot.create(:admin)
        card = FactoryBot.create(:card, creator: admin1)
        put card_path(card), headers: { 'Authorization': admin1.auth_token }, params: { card: { title: 'new title' } }

        expect(response).to have_http_status(:success)
        response_body = JSON.parse(response.body)
        expect(response_body['title']).to eq('new title')
      end

      it "Render error on another user card" do
        admin1 = FactoryBot.create(:admin)
        list1  = FactoryBot.create(:list, admin: admin1)
        card = FactoryBot.create(:card, list: list1)
        put card_path(card), headers: { 'Authorization': admin1.auth_token }, params: { card: { title: 'new title' } }
        expect(response).to have_http_status(:unauthorized)
      end
    end

    context "As a member" do
      it "Update his own card" do
        member = FactoryBot.create(:member)
        card = FactoryBot.create(:card, creator: member)
        put card_path(card), headers: { 'Authorization': member.auth_token }, params: { card: { title: 'new title' } }

        expect(response).to have_http_status(:success)
        response_body = JSON.parse(response.body)
        expect(response_body['title']).to eq('new title')
      end

      it "Render error on another user card" do
        member = FactoryBot.create(:member)
        card = FactoryBot.create(:card)
        put card_path(card), headers: { 'Authorization': member.auth_token }, params: { card: { title: 'new title' } }
        expect(response).to have_http_status(:unauthorized)
      end
    end

    context "As a guest" do
      it "Render unauthorized" do
        card = FactoryBot.create(:card)
        put card_path(card), params: { card: { title: 'new title' } }
        expect(response).to have_http_status(:unauthorized)
      end
    end
  end

  describe "DELETE /card/:id" do
    context 'As an Admin' do
      it "Delete his own card" do
        admin1 = FactoryBot.create(:admin)
        card = FactoryBot.create(:card, creator: admin1)
        delete card_path(card), headers: { 'Authorization': admin1.auth_token }
        expect(response).to have_http_status(:no_content)
        expect(Card.count).to eq(0)
      end

      it "Delete card in his own list" do
        admin1 = FactoryBot.create(:admin)
        list = FactoryBot.create(:list, admin: admin1)
        card = FactoryBot.create(:card, list: list)
        delete card_path(card), headers: { 'Authorization': admin1.auth_token }
        expect(response).to have_http_status(:no_content)
        expect(Card.count).to eq(0)
      end

      it "Render error on another admin List" do
        admin1 = FactoryBot.create(:admin)
        card = FactoryBot.create(:card)
        delete card_path(card), headers: { 'Authorization': admin1.auth_token }
        expect(response).to have_http_status(:unauthorized)
        expect(Card.count).to eq(1)
      end
    end

    context "As a member" do
      it "Update his own card" do
        member = FactoryBot.create(:member)
        card = FactoryBot.create(:card, creator: member)
        delete card_path(card), headers: { 'Authorization': member.auth_token }, params: { card: { title: 'new title' } }

        expect(response).to have_http_status(:no_content)
        expect(Card.count).to eq(0)
      end

      it "Render error on another user card" do
        member = FactoryBot.create(:member)
        card = FactoryBot.create(:card)
        delete card_path(card), headers: { 'Authorization': member.auth_token }, params: { card: { title: 'new title' } }
        expect(response).to have_http_status(:unauthorized)
        expect(Card.count).to eq(1)
      end
    end

    context "As a guest" do
      it "Render unauthorized" do
        card = FactoryBot.create(:card)
        delete card_path(card)
        expect(response).to have_http_status(:unauthorized)
        expect(Card.count).to eq(1)
      end
    end
  end

  describe "GET /card/:id" do
    context 'As an Admin' do
      it "Show Card" do
        admin1 = FactoryBot.create(:admin)
        card = FactoryBot.create(:card)
        comments = FactoryBot.create_list(:comment, 4, commentable: card)
        get card_path(card), headers: { 'Authorization': admin1.auth_token }
        expect(response).to have_http_status(:success)
        response_body = JSON.parse(response.body)
        expect(response_body['card']['title']).to eq(card.title)
        expect(response_body['comments'].length).to eq(3)
        expect(response_body['comments'][0]['id']).to eq(comments[0].id)
        expect(response_body['comments'][1]['id']).to eq(comments[1].id)
        expect(response_body['comments'][2]['id']).to eq(comments[2].id)
      end
    end

    context "As a member" do
      it 'Render unauthorized for non member list' do
        list1  = FactoryBot.create(:list)
        list2  = FactoryBot.create(:list)
        card = FactoryBot.create(:card, list: list2)
        member = FactoryBot.create(:member)
        list1.members << member
        get card_path(card), headers: { 'Authorization': member.auth_token }
        expect(response).to have_http_status(:not_found)
      end

      it "Show Card" do
        list1 = FactoryBot.create(:list)
        card = FactoryBot.create(:card, list: list1)
        member = FactoryBot.create(:member)
        list1.members << member
        get card_path(card), headers: { 'Authorization': member.auth_token }
        expect(response).to have_http_status(:success)
        response_body = JSON.parse(response.body)
        expect(response_body['card']['title']).to eq(card.title)
      end
    end

    context "As a guest" do
      it "Render unauthorized" do
        card = FactoryBot.create(:card)
        get card_path(card)
        expect(response).to have_http_status(:unauthorized)
      end
    end
  end
end
