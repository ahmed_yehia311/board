require 'rails_helper'

RSpec.describe "Lists", type: :request do
  describe "GET /lists" do
    context 'As an Admin' do
      it "Reads all lists" do
        admin1 = FactoryBot.create(:admin)
        admin2 = FactoryBot.create(:admin)
        list1  = FactoryBot.create(:list, admin: admin1)
        list2  = FactoryBot.create(:list, admin: admin2)
        get lists_path, headers: { 'Authorization': admin1.auth_token }
        expect(response).to have_http_status(:success)
        response_body = JSON.parse(response.body)
        expect(response_body['lists'].length).to eq(2)
        expect(response_body['lists'][0]['title']).to eq(list1.title)
        expect(response_body['lists'][1]['title']).to eq(list2.title)
      end
    end

    context "As a member" do
      it "Read only lists assigned to him" do
        admin1 = FactoryBot.create(:admin)
        member = FactoryBot.create(:member)
        list1  = FactoryBot.create(:list, admin: admin1)
        FactoryBot.create(:list, admin: admin1)
        list1.members << member
        get lists_path, headers: { 'Authorization': member.auth_token }
        expect(response).to have_http_status(:success)
        response_body = JSON.parse(response.body)
        expect(response_body['lists'].length).to eq(1)
        expect(response_body['lists'][0]['title']).to eq(list1.title)
      end
    end

    context "As a guest" do
      it "Render unauthorized" do
        FactoryBot.create(:list)
        get lists_path
        expect(response).to have_http_status(:unauthorized)
      end
    end
  end

  describe "POST /lists" do
    context 'As an Admin' do
      it "Create List" do
        admin1 = FactoryBot.create(:admin)
        post lists_path, headers: { 'Authorization': admin1.auth_token }, params: { list: { title: 'test title' } }

        expect(response).to have_http_status(:success)
        response_body = JSON.parse(response.body)
        expect(response_body['title']).to eq('test title')
      end
    end

    context "As a member" do
      it "Render unauthorized" do
        member = FactoryBot.create(:member)
        post lists_path, headers: { 'Authorization': member.auth_token }, params: { list: { title: 'test title' } }
        expect(response).to have_http_status(:unauthorized)
      end
    end

    context "As a guest" do
      it "Render unauthorized" do
        FactoryBot.create(:list)
        get lists_path
        expect(response).to have_http_status(:unauthorized)
      end
    end
  end

  describe "Update /list/:id" do
    context 'As an Admin' do
      it "Update his own List" do
        admin1 = FactoryBot.create(:admin)
        list1  = FactoryBot.create(:list, admin: admin1)
        put list_path(list1), headers: { 'Authorization': admin1.auth_token }, params: { list: { title: 'new title' } }

        expect(response).to have_http_status(:success)
        response_body = JSON.parse(response.body)
        expect(response_body['title']).to eq('new title')
      end

      it "Render error on another admin List" do
        admin1 = FactoryBot.create(:admin)
        list1  = FactoryBot.create(:list)
        put list_path(list1), headers: { 'Authorization': admin1.auth_token }, params: { list: { title: 'new title' } }
        expect(response).to have_http_status(:unauthorized)
      end
    end

    context "As a member" do
      it "Render unauthorized" do
        admin1 = FactoryBot.create(:admin)
        list1  = FactoryBot.create(:list, admin: admin1)
        member = FactoryBot.create(:member)
        put list_path(list1), headers: { 'Authorization': member.auth_token }, params: { list: { title: 'new title' } }
        expect(response).to have_http_status(:unauthorized)
      end
    end

    context "As a guest" do
      it "Render unauthorized" do
        list1 = FactoryBot.create(:list)
        put list_path(list1), params: { list: { title: 'new title' } }
        expect(response).to have_http_status(:unauthorized)
      end
    end
  end

  describe "DELETE /list/:id" do
    context 'As an Admin' do
      it "Delete his own List" do
        admin1 = FactoryBot.create(:admin)
        list1  = FactoryBot.create(:list, admin: admin1)
        delete list_path(list1), headers: { 'Authorization': admin1.auth_token }

        expect(response).to have_http_status(:no_content)
        expect(List.count).to eq(0)
      end

      it "Render error on another admin List" do
        admin1 = FactoryBot.create(:admin)
        list1  = FactoryBot.create(:list)
        delete list_path(list1), headers: { 'Authorization': admin1.auth_token }
        expect(response).to have_http_status(:unauthorized)
      end
    end

    context "As a member" do
      it "Render unauthorized" do
        admin1 = FactoryBot.create(:admin)
        list1  = FactoryBot.create(:list, admin: admin1)
        member = FactoryBot.create(:member)
        delete list_path(list1), headers: { 'Authorization': member.auth_token }
        expect(response).to have_http_status(:unauthorized)
      end
    end

    context "As a guest" do
      it "Render unauthorized" do
        list1 = FactoryBot.create(:list)
        delete list_path(list1)
        expect(response).to have_http_status(:unauthorized)
      end
    end
  end

  describe "GET /list/:id" do
    context 'As an Admin' do
      it "Show List" do
        admin1 = FactoryBot.create(:admin)
        list1  = FactoryBot.create(:list, admin: admin1)
        get list_path(list1), headers: { 'Authorization': admin1.auth_token }
        expect(response).to have_http_status(:success)
        response_body = JSON.parse(response.body)
        expect(response_body['list']['title']).to eq(list1.title)
      end

      it "Show cards on list paginaited and orderd by main comments" do
        admin1 = FactoryBot.create(:admin)
        list1  = FactoryBot.create(:list, admin: admin1)
        member = FactoryBot.create(:member)
        card_1 = FactoryBot.create(:card, list: list1, creator: member)
        card_2 = FactoryBot.create(:card, list: list1, creator: member)
        card_3 = FactoryBot.create(:card, list: list1, creator: member)
        FactoryBot.create_list(:card, 20, list: list1, creator: member)
        FactoryBot.create_list(:comment, 5, commentable: card_2, creator: member)
        FactoryBot.create_list(:comment, 2, commentable: card_3, creator: member)
        FactoryBot.create(:comment, commentable: card_1, creator: member)
        # Add subcomments to a comment in the second card to make sure orderd is by main comments only
        card3_first_comment = card_3.comments.first
        FactoryBot.create_list(:comment, 5, commentable: card3_first_comment, creator: member)
        get list_path(list1), headers: { 'Authorization': admin1.auth_token }
        response_body = JSON.parse(response.body)
        expect(response_body['cards_total_pages']).to eq(3)
        expect(response_body['cards_page']).to eq(1)
        expect(response_body['cards'][0]['title']).to eq(card_2.title)
        expect(response_body['cards'][0]['comments_count']).to eq(5)
        expect(response_body['cards'][1]['title']).to eq(card_3.title)
        expect(response_body['cards'][1]['comments_count']).to eq(2)
        expect(response_body['cards'][2]['title']).to eq(card_1.title)
        expect(response_body['cards'][2]['comments_count']).to eq(1)
      end
    end

    context "As a member" do
      it 'Render unauthorized for non member list' do
        admin1 = FactoryBot.create(:admin)
        list1  = FactoryBot.create(:list, admin: admin1)
        member = FactoryBot.create(:member)
        get list_path(list1), headers: { 'Authorization': member.auth_token }
        expect(response).to have_http_status(:not_found)
      end

      it "Show List" do
        admin1 = FactoryBot.create(:admin)
        list1  = FactoryBot.create(:list, admin: admin1)
        member = FactoryBot.create(:member)
        list1.members << member
        get list_path(list1), headers: { 'Authorization': member.auth_token }
        expect(response).to have_http_status(:success)
        response_body = JSON.parse(response.body)
        expect(response_body['list']['title']).to eq(list1.title)
      end
    end

    context "As a guest" do
      it "Render unauthorized" do
        list1 = FactoryBot.create(:list)
        get list_path(list1)
        expect(response).to have_http_status(:unauthorized)
      end
    end
  end

  describe "PUT /list/:id/assign/:member_id" do
    context 'As an Admin' do
      it "Assign Member to list" do
        admin1 = FactoryBot.create(:admin)
        list1  = FactoryBot.create(:list, admin: admin1)
        member = FactoryBot.create(:member)
        put list_assign_member_path(list1, member), headers: { 'Authorization': admin1.auth_token }
        expect(response).to have_http_status(:created)
        expect(list1.members.count).to eq(1)
      end

      it "Render unauthorized for not owned list" do
        admin1 = FactoryBot.create(:admin)
        list1  = FactoryBot.create(:list)
        member = FactoryBot.create(:member)
        put list_assign_member_path(list1, member), headers: { 'Authorization': admin1.auth_token }
        expect(response).to have_http_status(:unauthorized)
        expect(list1.members.count).to eq(0)
      end

      it "Render error for already assigned member" do
        admin1 = FactoryBot.create(:admin)
        list1  = FactoryBot.create(:list, admin: admin1)
        member = FactoryBot.create(:member)
        list1.members << member
        put list_assign_member_path(list1, member), headers: { 'Authorization': admin1.auth_token }
        expect(response).to have_http_status(:unprocessable_entity)
        expect(list1.members.count).to eq(1)
      end
    end

    context "As a member" do
      it 'Render unauthorized for non member list' do
        admin1 = FactoryBot.create(:admin)
        list1  = FactoryBot.create(:list, admin: admin1)
        member = FactoryBot.create(:member)
        put list_assign_member_path(list1, member), headers: { 'Authorization': member.auth_token }
        expect(response).to have_http_status(:unauthorized)
      end
    end

    context "As a guest" do
      it "Render unauthorized" do
        list1  = FactoryBot.create(:list)
        member = FactoryBot.create(:member)
        put list_assign_member_path(list1, member)
        expect(response).to have_http_status(:unauthorized)
      end
    end
  end

  describe "PUT /list/:id/unassign/:member_id" do
    context 'As an Admin' do
      it "Unassign Member to list" do
        admin1 = FactoryBot.create(:admin)
        list1  = FactoryBot.create(:list, admin: admin1)
        member = FactoryBot.create(:member)
        list1.members << member
        put list_unassign_member_path(list1, member), headers: { 'Authorization': admin1.auth_token }
        expect(response).to have_http_status(:success)
        expect(list1.members.count).to eq(0)
      end

      it "Render unauthorized for not owned list" do
        admin1 = FactoryBot.create(:admin)
        list1  = FactoryBot.create(:list)
        member = FactoryBot.create(:member)
        list1.members << member
        put list_unassign_member_path(list1, member), headers: { 'Authorization': admin1.auth_token }
        expect(response).to have_http_status(:unauthorized)
        expect(list1.members.count).to eq(1)
      end

      it "Render error for not assigned member" do
        admin1 = FactoryBot.create(:admin)
        list1  = FactoryBot.create(:list, admin: admin1)
        member = FactoryBot.create(:member)
        put list_unassign_member_path(list1, member), headers: { 'Authorization': admin1.auth_token }
        expect(response).to have_http_status(:unprocessable_entity)
        expect(list1.members.count).to eq(0)
      end
    end

    context "As a member" do
      it 'Render unauthorized for non member list' do
        admin1 = FactoryBot.create(:admin)
        list1  = FactoryBot.create(:list, admin: admin1)
        member = FactoryBot.create(:member)
        list1.members << member
        put list_unassign_member_path(list1, member), headers: { 'Authorization': member.auth_token }
        expect(response).to have_http_status(:unauthorized)
      end
    end

    context "As a guest" do
      it "Render unauthorized" do
        list1  = FactoryBot.create(:list)
        member = FactoryBot.create(:member)
        list1.members << member
        put list_assign_member_path(list1, member)
        expect(response).to have_http_status(:unauthorized)
      end
    end
  end
end
