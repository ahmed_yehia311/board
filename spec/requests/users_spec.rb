require 'rails_helper'

RSpec.describe "Users", type: :request do
  describe "GET /users" do
    it "Returns users list" do
      FactoryBot.create_list(:member, 20)
      get users_path
      expect(response).to have_http_status(:success)
      response_body = JSON.parse(response.body)
      expect(response_body['users'].length).to eq(10)
      expect(response_body['page']).to eq(1)
      expect(response_body['total_pages']).to eq(2)
      expect(response_body['users'][0]['email']).to eq(User.first.email)
    end
  end
end
