require 'rails_helper'

RSpec.describe "Comments", type: :request do
  describe "GET /card/:id/comments" do
    context 'As an Admin' do
      it "Reads all comments" do
        admin1 = FactoryBot.create(:admin)
        card = FactoryBot.create(:card)
        comments = FactoryBot.create_list(:comment, 15, commentable: card, list: card.list)
        get card_comments_path(card), headers: { 'Authorization': admin1.auth_token }
        expect(response).to have_http_status(:success)
        response_body = JSON.parse(response.body)
        expect(response_body['comments'].length).to eq(10)
        expect(response_body['comments'][0]['content']).to eq(comments[0]['content'])
        expect(response_body['total_pages']).to eq(2)
        expect(response_body['page']).to eq(1)
      end
    end

    context "As a member" do
      it "Read comments belongs to assigned lists" do
        admin1 = FactoryBot.create(:admin)
        member = FactoryBot.create(:member)
        list1  = FactoryBot.create(:list, admin: admin1)
        list2  = FactoryBot.create(:list, admin: admin1)
        list1.members << member
        card1 = FactoryBot.create(:card, creator: member, list: list1)
        card2 = FactoryBot.create(:card, creator: member, list: list2)
        comments1 = FactoryBot.create_list(:comment, 15, commentable: card1, list: card1.list)
        FactoryBot.create_list(:comment, 15, commentable: card2, list: card2.list)
        get card_comments_path(card1), headers: { 'Authorization': member.auth_token }
        expect(response).to have_http_status(:success)
        response_body = JSON.parse(response.body)
        expect(response_body['comments'].length).to eq(10)
        expect(response_body['comments'][0]['content']).to eq(comments1[0]['content'])
        expect(response_body['total_pages']).to eq(2)
        expect(response_body['page']).to eq(1)
      end

      it "Render error on comments belongs to unassigned lists" do
        admin1 = FactoryBot.create(:admin)
        member = FactoryBot.create(:member)
        list1  = FactoryBot.create(:list, admin: admin1)
        list2  = FactoryBot.create(:list, admin: admin1)
        list1.members << member
        card1 = FactoryBot.create(:card, creator: member, list: list1)
        card2 = FactoryBot.create(:card, creator: member, list: list2)
        FactoryBot.create_list(:comment, 15, commentable: card1, list: card1.list)
        FactoryBot.create_list(:comment, 15, commentable: card2, list: card2.list)
        get card_comments_path(card2), headers: { 'Authorization': member.auth_token }
        expect(response).to have_http_status(:not_found)
      end
    end

    context "As a guest" do
      it "Render unauthorized" do
        card = FactoryBot.create(:card)
        get card_comments_path(card)
        expect(response).to have_http_status(:unauthorized)
      end
    end
  end

  describe "GET /comment/:id/comments" do
    context 'As an Admin' do
      it "Reads all comments" do
        admin1 = FactoryBot.create(:admin)
        card = FactoryBot.create(:card)
        comment = FactoryBot.create(:comment, commentable: card, list: card.list)
        comments = FactoryBot.create_list(:comment, 15, commentable: comment, list: comment.list)
        get comment_comments_path(comment), headers: { 'Authorization': admin1.auth_token }
        expect(response).to have_http_status(:success)
        response_body = JSON.parse(response.body)
        expect(response_body['comments'].length).to eq(10)
        expect(response_body['comments'][0]['content']).to eq(comments[0]['content'])
        expect(response_body['total_pages']).to eq(2)
        expect(response_body['page']).to eq(1)
      end
    end

    context "As a member" do
      it "Read comments belongs to assigned lists" do
        admin1 = FactoryBot.create(:admin)
        member = FactoryBot.create(:member)
        list1  = FactoryBot.create(:list, admin: admin1)
        list2  = FactoryBot.create(:list, admin: admin1)
        list1.members << member
        card1 = FactoryBot.create(:card, creator: member, list: list1)
        FactoryBot.create(:card, creator: member, list: list2)
        comment = FactoryBot.create(:comment, commentable: card1, list: list1)
        comments = FactoryBot.create_list(:comment, 15, commentable: comment, list: list1)
        get comment_comments_path(comment), headers: { 'Authorization': member.auth_token }
        expect(response).to have_http_status(:success)
        response_body = JSON.parse(response.body)
        expect(response_body['comments'].length).to eq(10)
        expect(response_body['comments'][0]['content']).to eq(comments[0]['content'])
        expect(response_body['total_pages']).to eq(2)
        expect(response_body['page']).to eq(1)
      end

      it "Render error on comments belongs to unassigned lists" do
        admin1 = FactoryBot.create(:admin)
        member = FactoryBot.create(:member)
        list1  = FactoryBot.create(:list, admin: admin1)
        list2  = FactoryBot.create(:list, admin: admin1)
        list1.members << member
        FactoryBot.create(:card, creator: member, list: list1)
        card2 = FactoryBot.create(:card, creator: member, list: list2)
        comment = FactoryBot.create(:comment, commentable: card2, list: card2.list)
        FactoryBot.create_list(:comment, 15, commentable: comment, list: comment.list)
        get comment_comments_path(comment), headers: { 'Authorization': member.auth_token }
        expect(response).to have_http_status(:not_found)
      end
    end

    context "As a guest" do
      it "Render unauthorized" do
        comment = FactoryBot.create(:comment)
        get comment_comments_path(comment)
        expect(response).to have_http_status(:unauthorized)
      end
    end
  end

  describe "POST /card/:card_id/comments" do
    context 'As an Admin' do
      it "Create Comment on card" do
        admin1 = FactoryBot.create(:admin)
        card = FactoryBot.create(:card)
        post card_comments_path(card), headers: { 'Authorization': admin1.auth_token }, params: { comment: { content: 'test commnet' } }

        expect(response).to have_http_status(:created)
        response_body = JSON.parse(response.body)
        expect(response_body['content']).to eq('test commnet')
      end
    end

    context "As a member" do
      it "Create Comment on card on assigned list" do
        list = FactoryBot.create(:list)
        member = FactoryBot.create(:member)
        list.members << member
        card = FactoryBot.create(:card, list: list)
        post card_comments_path(card), headers: { 'Authorization': member.auth_token }, params: { comment: { content: 'test commnet' } }
        expect(response).to have_http_status(:created)
        response_body = JSON.parse(response.body)
        expect(response_body['content']).to eq('test commnet')
      end

      it "Render unauthorized for not assigned list" do
        member = FactoryBot.create(:member)
        list = FactoryBot.create(:list)
        card = FactoryBot.create(:card, list: list)
        post card_comments_path(card), headers: { 'Authorization': member.auth_token }, params: { comment: { content: 'test commnet' } }
        expect(response).to have_http_status(:not_found)
      end
    end

    context "As a guest" do
      it "Render unauthorized" do
        card = FactoryBot.create(:card)
        post card_comments_path(card), params: { comment: { content: 'test commnet' } }
        expect(response).to have_http_status(:unauthorized)
      end
    end
  end

  describe "POST /comment/:comment_id/comments" do
    context 'As an Admin' do
      it "Create Comment on card" do
        admin1 = FactoryBot.create(:admin)
        comment = FactoryBot.create(:comment)
        post comment_comments_path(comment), headers: { 'Authorization': admin1.auth_token }, params: { comment: { content: 'test commnet' } }

        expect(response).to have_http_status(:created)
        response_body = JSON.parse(response.body)
        expect(response_body['content']).to eq('test commnet')
      end
    end

    context "As a member" do
      it "Create Comment on card on assigned list" do
        list = FactoryBot.create(:list)
        member = FactoryBot.create(:member)
        list.members << member
        card = FactoryBot.create(:card, list: list)
        comment = FactoryBot.create(:comment, commentable: card, list: card.list)
        post comment_comments_path(comment), headers: { 'Authorization': member.auth_token }, params: { comment: { content: 'test commnet' } }
        expect(response).to have_http_status(:created)
        response_body = JSON.parse(response.body)
        expect(response_body['content']).to eq('test commnet')
      end

      it "Render unauthorized for not assigned list" do
        member = FactoryBot.create(:member)
        list = FactoryBot.create(:list)
        card = FactoryBot.create(:card, list: list)
        comment = FactoryBot.create(:comment, commentable: card, list: card.list)
        post comment_comments_path(comment), headers: { 'Authorization': member.auth_token }, params: { comment: { content: 'test commnet' } }
        expect(response).to have_http_status(:not_found)
      end
    end

    context "As a guest" do
      it "Render unauthorized" do
        comment = FactoryBot.create(:comment)
        post comment_comments_path(comment), params: { comment: { content: 'test commnet' } }
        expect(response).to have_http_status(:unauthorized)
      end
    end
  end

  describe "Update /comment/:id" do
    context 'As an Admin' do
      it "Update his own comment" do
        admin1 = FactoryBot.create(:admin)
        comment = FactoryBot.create(:comment, creator: admin1)
        put comment_path(comment), headers: { 'Authorization': admin1.auth_token }, params: { comment: { content: 'new comment' } }

        expect(response).to have_http_status(:success)
        response_body = JSON.parse(response.body)
        expect(response_body['content']).to eq('new comment')
      end

      it "Update comment on his own list" do
        admin1 = FactoryBot.create(:admin)
        list = FactoryBot.create(:list, admin: admin1)
        comment = FactoryBot.create(:comment, list: list)
        put comment_path(comment), headers: { 'Authorization': admin1.auth_token }, params: { comment: { content: 'new comment' } }

        expect(response).to have_http_status(:success)
        response_body = JSON.parse(response.body)
        expect(response_body['content']).to eq('new comment')
      end

      it "Render error on others lists" do
        admin1 = FactoryBot.create(:admin)
        list = FactoryBot.create(:list)
        comment = FactoryBot.create(:comment, list: list)
        put comment_path(comment), headers: { 'Authorization': admin1.auth_token }, params: { comment: { content: 'new comment' } }

        expect(response).to have_http_status(:not_found)
      end
    end

    context "As a member" do
      it "Update his own comment" do
        member = FactoryBot.create(:member)
        list = FactoryBot.create(:list)
        list.members << member
        comment = FactoryBot.create(:comment, creator: member, list: list)
        put comment_path(comment), headers: { 'Authorization': member.auth_token }, params: { comment: { content: 'new comment' } }

        expect(response).to have_http_status(:success)
        response_body = JSON.parse(response.body)
        expect(response_body['content']).to eq('new comment')
      end

      it "Render error on another user comment" do
        member = FactoryBot.create(:member)
        comment = FactoryBot.create(:comment)
        put comment_path(comment), headers: { 'Authorization': member.auth_token }, params: { comment: { content: 'new comment' } }
        expect(response).to have_http_status(:not_found)
      end
    end

    context "As a guest" do
      it "Render unauthorized" do
        comment = FactoryBot.create(:comment)
        put comment_path(comment), params: { comment: { content: 'new comment' } }
        expect(response).to have_http_status(:unauthorized)
      end
    end
  end

  describe "DELETE /comment/:id" do
    context 'As an Admin' do
      it "Delete his own comment" do
        admin1 = FactoryBot.create(:admin)
        list = FactoryBot.create(:list)
        comment = FactoryBot.create(:comment, list: list, creator: admin1)
        delete comment_path(comment), headers: { 'Authorization': admin1.auth_token }
        expect(response).to have_http_status(:no_content)
        expect(Comment.count).to eq(0)
      end

      it "Delete card in his own list" do
        admin1 = FactoryBot.create(:admin)
        list = FactoryBot.create(:list, admin: admin1)
        comment = FactoryBot.create(:comment, list: list)
        delete comment_path(comment), headers: { 'Authorization': admin1.auth_token }
        expect(response).to have_http_status(:no_content)
        expect(Comment.count).to eq(0)
      end

      it "Render error on another admin List" do
        admin1 = FactoryBot.create(:admin)
        comment = FactoryBot.create(:comment)
        delete comment_path(comment), headers: { 'Authorization': admin1.auth_token }
        expect(response).to have_http_status(:not_found)
        expect(Comment.count).to eq(1)
      end
    end

    context "As a member" do
      it "Delete his own card" do
        member = FactoryBot.create(:member)
        list = FactoryBot.create(:list)
        list.members << member
        comment = FactoryBot.create(:comment, creator: member, list: list)
        delete comment_path(comment), headers: { 'Authorization': member.auth_token }
        expect(response).to have_http_status(:no_content)
        expect(Comment.count).to eq(0)
      end

      it "Delete comment with all its reply" do
        member = FactoryBot.create(:member)
        list = FactoryBot.create(:list)
        list.members << member
        comment = FactoryBot.create(:comment, creator: member, list: list)
        FactoryBot.create_list(:comment, 3, commentable: comment)
        delete comment_path(comment), headers: { 'Authorization': member.auth_token }
        expect(response).to have_http_status(:no_content)
        expect(Comment.count).to eq(0)
      end

      it "Render error on another user card" do
        member = FactoryBot.create(:member)
        list = FactoryBot.create(:list)
        list.members << member
        comment = FactoryBot.create(:comment, list: list)
        delete comment_path(comment), headers: { 'Authorization': member.auth_token }
        expect(response).to have_http_status(:unauthorized)
        expect(Comment.count).to eq(1)
      end
    end

    context "As a guest" do
      it "Render unauthorized" do
        comment = FactoryBot.create(:comment)
        delete comment_path(comment)
        expect(response).to have_http_status(:unauthorized)
        expect(Comment.count).to eq(1)
      end
    end
  end

  describe "GET /comment/:id" do
    context 'As an Admin' do
      it "Show Comment" do
        admin1 = FactoryBot.create(:admin)
        comment = FactoryBot.create(:comment)
        comments = FactoryBot.create_list(:comment, 14, commentable: comment)
        get comment_path(comment), headers: { 'Authorization': admin1.auth_token }
        expect(response).to have_http_status(:success)
        response_body = JSON.parse(response.body)
        expect(response_body['comment']['content']).to eq(comment.content)
        expect(response_body['replys'].length).to eq(10)
        expect(response_body['replys'][0]['id']).to eq(comments[0].id)
        expect(response_body['total_pages']).to eq(2)
        expect(response_body['page']).to eq(1)
      end
    end

    context "As a member" do
      it 'Render unauthorized for non member list' do
        list1  = FactoryBot.create(:list)
        list2  = FactoryBot.create(:list)
        card = FactoryBot.create(:card, list: list2)
        comment = FactoryBot.create(:comment, list: list2, commentable: card)
        member = FactoryBot.create(:member)
        list1.members << member
        get comment_path(comment), headers: { 'Authorization': member.auth_token }
        expect(response).to have_http_status(:not_found)
      end

      it "Show Card" do
        list1  = FactoryBot.create(:list)
        card = FactoryBot.create(:card, list: list1)
        comment = FactoryBot.create(:comment, list: list1, commentable: card)
        member = FactoryBot.create(:member)
        list1.members << member
        get comment_path(comment), headers: { 'Authorization': member.auth_token }
        expect(response).to have_http_status(:success)
        response_body = JSON.parse(response.body)
        expect(response_body['comment']['content']).to eq(comment.content)
      end
    end

    context "As a guest" do
      it "Render unauthorized" do
        comment = FactoryBot.create(:comment)
        get comment_path(comment)
        expect(response).to have_http_status(:unauthorized)
      end
    end
  end
end
