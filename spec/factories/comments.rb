# == Schema Information
#
# Table name: comments
#
#  commentable_id   :bigint(8)
#  commentable_type :string
#  comments_count   :integer          default(0)
#  content          :text
#  created_at       :datetime         not null
#  creator_id       :bigint(8)
#  creator_type     :string
#  id               :bigint(8)        not null, primary key
#  list_id          :bigint(8)
#  updated_at       :datetime         not null
#
# Indexes
#
#  index_comments_on_commentable_type_and_commentable_id  (commentable_type,commentable_id)
#  index_comments_on_comments_count                       (comments_count)
#  index_comments_on_creator_type_and_creator_id          (creator_type,creator_id)
#  index_comments_on_list_id                              (list_id)
#

FactoryBot.define do
  factory :comment do
    content { FFaker::Book.description }
    list
    association :creator, factory: :admin
    association :commentable, factory: :card
  end
end
