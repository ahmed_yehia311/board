# == Schema Information
#
# Table name: users
#
#  created_at      :datetime         not null
#  email           :string           not null
#  id              :bigint(8)        not null, primary key
#  password_digest :string           not null
#  type            :string           not null
#  updated_at      :datetime         not null
#  username        :string           not null
#
# Indexes
#
#  index_users_on_email     (email) UNIQUE
#  index_users_on_username  (username) UNIQUE
#

FactoryBot.define do
  factory :user do
    sequence(:email) { |i| "test-#{i}@board.com" }
    sequence(:username) { |i| "test-#{i}" }
    password { '12345678' }
  end

  factory :member, class: Member, parent: :user do
  end

  factory :admin, class: Admin, parent: :user do
  end
end
