# == Schema Information
#
# Table name: cards
#
#  comments_count :integer          default(0)
#  created_at     :datetime         not null
#  creator_id     :bigint(8)
#  creator_type   :string
#  description    :text
#  id             :bigint(8)        not null, primary key
#  list_id        :bigint(8)
#  title          :string
#  updated_at     :datetime         not null
#
# Indexes
#
#  index_cards_on_comments_count               (comments_count)
#  index_cards_on_creator_type_and_creator_id  (creator_type,creator_id)
#  index_cards_on_list_id                      (list_id)
#
# Foreign Keys
#
#  fk_rails_...  (list_id => lists.id)
#

FactoryBot.define do
  factory :card do
    title { FFaker::Book.title }
    description { FFaker::Book.description }
    list
    association :creator, factory: :admin
  end
end
