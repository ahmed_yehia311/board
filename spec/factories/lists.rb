# == Schema Information
#
# Table name: lists
#
#  admin_id   :bigint(8)
#  created_at :datetime         not null
#  id         :bigint(8)        not null, primary key
#  title      :string
#  updated_at :datetime         not null
#
# Indexes
#
#  index_lists_on_admin_id  (admin_id)
#

FactoryBot.define do
  factory :list do
    title { FFaker::Book.title }
    admin
  end
end
