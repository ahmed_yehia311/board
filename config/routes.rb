Rails.application.routes.draw do
  scope module: :api, defaults: { format: :json } do
    scope module: :v1 do
      post 'sign_in', to: 'authentication#authenticate'
      post 'sign_up', to: 'users#create'
      resources :users, only: [:index]
      resources :lists do
        put 'assign/:member_id', to: 'lists#assign_member', as: :assign_member
        put 'unassign/:member_id', to: 'lists#unassign_member', as: :unassign_member
      end
      resources :cards do
        get '/comments', to: 'comments#index', as: :comments
        post '/comments', to: 'comments#create'
      end
      resources :comments, only: %i[ create update destroy show] do
        get '/comments', to: 'comments#index', as: :comments
        post '/comments', to: 'comments#create'
      end
    end
  end

end
