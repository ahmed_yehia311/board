json.cards do
  json.array! @cards
end
json.total_pages @cards.total_pages
json.page @cards.current_page
