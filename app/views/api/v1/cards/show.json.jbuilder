json.card do
  json.title @card.title
  json.description @card.description
  json.list_id @card.list_id
  json.list_title @card.list_title
end

json.comments do
  json.array! @comments, :id, :content, :creator_type, :creator_id, :creator_username, :comments_count
end
