json.lists do
  json.array! @lists, :id, :title, :admin_id
end
json.total_pages @lists.total_pages
json.page @lists.current_page
