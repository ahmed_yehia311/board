json.list do
  json.title @list.title
  json.admin_id @list.admin_id
end

json.cards do
  json.array! @cards
end
json.cards_total_pages @cards.total_pages
json.cards_page @cards.current_page
