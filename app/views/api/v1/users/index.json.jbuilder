json.users do
  json.array! @users, :id, :username, :email, :type
end
json.total_pages @users.total_pages
json.page @users.current_page
