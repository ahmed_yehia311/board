json.comments do
  json.array! @comments
end
json.total_pages @comments.total_pages
json.page @comments.current_page
