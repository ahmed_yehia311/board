json.comment @comment
json.replys do
  json.array! @replys
end
json.total_pages @replys.total_pages
json.page @replys.current_page
