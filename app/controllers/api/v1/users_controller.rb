module Api
  module V1
    class UsersController < ApiController
      skip_before_action :authenticate_request

      def create
        user = User.new(user_params)
        if user.save
          render json: user.to_json(only: [:id, :username, :email], methods: [:auth_token] ), status: :created
        else
          render_error(user.errors.full_messages[0], :unprocessable_entity)
        end
      end

      def index
        @users = User.select(:id, :username, :email, :type).page(params[:page])
      end

      private

      def user_params
        params.require(:user).permit(:email, :password, :username, :type)
      end
    end
  end
end
