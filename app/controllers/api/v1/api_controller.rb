module Api
  module V1
    class ApiController < ApplicationController
      include Concerns::ErrorHandler
      before_action :authenticate_request

      attr_reader :current_user

      rescue_from ActionController::ParameterMissing, with: :params_missing

      private

      def authenticate_request
        @current_user = AuthenticateApiRequestService.new(request.headers).call
        render_unauthorized unless @current_user
      end

      def authorize_admin_user
        render_unauthorized unless @current_user.is_a? Admin
      end

      def params_missing
        render_error('param is missing or the value is empty', :unprocessable_entity)
      end
    end
  end
end
