module Api
  module V1
    class CommentsController < ApiController
      before_action :load_resource, only: %i[index create]

      def index
        @comments = @resource.comments.order(created_at: :asc).page(params[:page])
      end

      def show
        @comment = ::CommentPolicy.scope(current_user).find(params[:id])
        @replys = @comment.comments.order(created_at: :asc).page(params[:page])
      end

      def create
        comment = Comment.new(comment_params)
        comment.commentable = @resource
        comment.creator = current_user
        comment.list = @resource.list

        return render_unauthorized unless ::CommentPolicy.new(@current_user, comment).create?

        if comment.save
          render json: comment, status: :created
        else
          render_error(comment.errors.full_messages[0], :unprocessable_entity)
        end
      end

      def update
        comment = ::CommentPolicy.scope(current_user).find(params[:id])

        return render_unauthorized unless ::CommentPolicy.new(@current_user, comment).update?

        if comment.update(comment_params)
          render json: comment
        else
          render_error(comment.errors.full_messages[0], :unprocessable_entity)
        end
      end

      def destroy
        comment = ::CommentPolicy.scope(current_user).find(params[:id])

        return render_unauthorized unless ::CommentPolicy.new(@current_user, comment).destroy?

        comment.destroy
        head :no_content
      end

      private

      def comment_params
        params.require(:comment).permit(:content)
      end

      def load_resource
        return render_error('Resource Missing') if params[:comment_id].nil? && params[:card_id].nil?

        @resource = if params[:comment_id].present?
                      ::CommentPolicy.scope(current_user).find(params[:comment_id])
                    else
                      ::CardPolicy.scope(current_user).find(params[:card_id])
                    end
      end
    end
  end
end
