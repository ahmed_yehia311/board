module Api
  module V1
    class AuthenticationController < ApiController
      skip_before_action :authenticate_request

      def authenticate
        if params[:email].nil? || params[:password].nil?
          return render_error('No email or password', :unprocessable_entity)
        end

        token = AuthenticationService.new(params[:email], params[:password]).call

        if token.present?
          render json: { auth_token: token }
        else
          render_error('invalid credentials', :unauthorized)
        end
      end
    end
  end
end
