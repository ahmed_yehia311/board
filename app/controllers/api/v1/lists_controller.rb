module Api
  module V1
    class ListsController < ApiController
      before_action :authorize_admin_user, except: %i[index show]

      def index
        @lists = ::ListPolicy.scope(current_user).select(:title, :id, :admin_id).page(params[:page])
      end

      def create
        list = List.new(list_params)
        list.admin = @current_user
        if list.save
          render json: list.to_json(only: [:title, :id]), status: :created
        else
          render_error(list.errors.full_messages[0], :unprocessable_entity)
        end
      end

      def show
        @list = ::ListPolicy.scope(current_user).find(params[:id])
        @cards = @list.cards.higher_comments.page(params[:page])
      end

      def update
        list = ::ListPolicy.scope(current_user).find(params[:id])

        return render_unauthorized unless ::ListPolicy.new(@current_user, list).update?

        if list.update(list_params)
          render json: list.to_json(only: [:title, :id])
        else
          render_error(list.errors.full_messages[0], :unprocessable_entity)
        end
      end

      def destroy
        list = ::ListPolicy.scope(current_user).find(params[:id])

        return render_unauthorized unless ::ListPolicy.new(@current_user, list).destroy?

        list.destroy
        head :no_content
      end

      def assign_member
        list = ::ListPolicy.scope(current_user).find(params[:list_id])
        member = Member.find(params[:member_id])
        return render_unauthorized unless ::ListPolicy.new(@current_user, list).assign?

        if list.members.find_by(id: member.id).present?
          render_error('Member Already Assigned', :unprocessable_entity)
        else
          list.members << member
          render json: { status: 'success' }, status: :created
        end
      end

      def unassign_member
        list = ::ListPolicy.scope(current_user).find(params[:list_id])
        member = Member.find(params[:member_id])
        return render_unauthorized unless ::ListPolicy.new(@current_user, list).unassign?

        if list.members.find_by(id: member.id).present?
          list.members.delete(member)
          render json: { status: 'success' }
        else
          render_error('Member Already UnAssigned', :unprocessable_entity)
        end
      end

      private

      def list_params
        params.require(:list).permit(:title)
      end
    end
  end
end
