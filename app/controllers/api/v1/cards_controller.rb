module Api
  module V1
    class CardsController < ApiController

      def index
        @cards = ::CardPolicy.scope(current_user).where(creator: current_user).higher_comments.page(params[:page])
      end

      def show
        @card = ::CardPolicy.scope(current_user).find(params[:id])
        @comments = @card.comments.includes(:creator).first(3)
      end

      def create
        card = Card.new(card_params)
        card.creator = @current_user
        return render_unauthorized unless ::CardPolicy.new(@current_user, card).create?

        if card.save
          render json: card.to_json(only: [:id, :title, :description, :list_id], methods: [:list_title]), status: :created
        else
          render_error(card.errors.full_messages[0], :unprocessable_entity)
        end
      end

      def update
        card = Card.find(params[:id])
        return render_unauthorized unless ::CardPolicy.new(@current_user, card).update?

        if card.update(card_update_params)
          render json: card.to_json(only: [:id, :title, :description, :list_id], methods: [:list_title])
        else
          render_error(card.errors.full_messages[0], :unprocessable_entity)
        end
      end

      def destroy
        card = Card.find(params[:id])
        return render_unauthorized unless ::CardPolicy.new(@current_user, card).destroy?

        card.destroy
        head :no_content
      end

      private

      def card_params
        params.require(:card).permit(:title, :description, :list_id)
      end

      def card_update_params
        params.require(:card).permit(:title, :description)
      end
    end
  end
end
