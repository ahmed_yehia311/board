# == Schema Information
#
# Table name: lists
#
#  admin_id   :bigint(8)
#  created_at :datetime         not null
#  id         :bigint(8)        not null, primary key
#  title      :string
#  updated_at :datetime         not null
#
# Indexes
#
#  index_lists_on_admin_id  (admin_id)
#

class List < ApplicationRecord
  paginates_per 10

  # Associations
  has_and_belongs_to_many :members, association_foreign_key: "user_id"
  belongs_to :admin
  has_many :cards

  # Validations
  validates :title, presence: true
  validates :admin, presence: true
end
