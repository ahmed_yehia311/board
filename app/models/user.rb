# == Schema Information
#
# Table name: users
#
#  created_at      :datetime         not null
#  email           :string           not null
#  id              :bigint(8)        not null, primary key
#  password_digest :string           not null
#  type            :string           not null
#  updated_at      :datetime         not null
#  username        :string           not null
#
# Indexes
#
#  index_users_on_email     (email) UNIQUE
#  index_users_on_username  (username) UNIQUE
#

class User < ApplicationRecord
  paginates_per 10

  has_secure_password

  # Validations
  validates :email, presence: true, uniqueness: true
  validates :username, presence: true, uniqueness: true
  validates :type, presence: true

  # Associations
  has_many :cards, as: :creator
  has_many :comments, as: :creator

  def auth_token
    JsonWebToken.encode(user_id: id)
  end

  def admin?
    is_a? Admin
  end

  def member?
    is_a? Member
  end
end
