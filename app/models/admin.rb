# == Schema Information
#
# Table name: users
#
#  created_at      :datetime         not null
#  email           :string           not null
#  id              :bigint(8)        not null, primary key
#  password_digest :string           not null
#  type            :string           not null
#  updated_at      :datetime         not null
#  username        :string           not null
#
# Indexes
#
#  index_users_on_email     (email) UNIQUE
#  index_users_on_username  (username) UNIQUE
#

class Admin < User
  # Associations
  has_many :lists
end
