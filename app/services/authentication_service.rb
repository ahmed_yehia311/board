class AuthenticationService
  def initialize(email, password)
    @email = email
    @password = password
  end

  def call
    user.auth_token if user
  end

  private

  attr_accessor :email, :password

  def user
    user = User.find_by_email(email)
    user if(user && user.authenticate(password))
  end
end
