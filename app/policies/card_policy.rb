class CardPolicy < ApplicationPolicy
  def create?
    @user.admin? || (@user.member? && @user.lists.find_by(id: @record.list_id).present?)
  end

  def update?
    @record.creator == @user
  end

  def destroy?
    @record.creator == @user || (@user.admin? && @record.list.admin == @user)
  end

  def self.scope(user)
    if user.admin?
      Card
    else
      Card.where(list_id: user.lists)
    end
  end
end
