class ApplicationPolicy
  attr_reader :user, :record

  def initialize(current_user, record)
    @user = current_user
    @record = record
  end
end
