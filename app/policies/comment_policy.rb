class CommentPolicy < ApplicationPolicy
  def create?
    @user.admin? || (@user.member? && @user.lists.find(@record.list_id).present?)
  end

  def update?
    (@user.admin? && (@record.creator.eql?(@user) || @user.lists.find(@record.list_id).present?)) || (@record.creator == @user)
  end

  def destroy?
    update?
  end

  def self.scope(user)
    if user.admin?
      Comment
    else
      Comment.where(list_id: user.lists)
    end
  end
end
