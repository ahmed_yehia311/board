class ListPolicy < ApplicationPolicy
  def update?
    @record.admin == @user
  end

  def destroy?
    update?
  end

  def assign?
    update?
  end

  def unassign?
    update?
  end

  def self.scope(user)
    if user.admin?
      List
    else
      user.lists
    end
  end
end
